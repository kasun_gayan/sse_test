import React from 'react'
import { Card} from 'react-bootstrap'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import HighchartsMore from 'highcharts/highcharts-more';
import ApiService from './../services/ApiService';
import Loader from 'react-loader-spinner';
import _ from 'lodash';

class Home extends React.Component{

  constructor(props){
    super(props);
    this.state ={loading: false, filters: {}, studentIds: [], subjects: [], years: [], grades: []};
    this.handleStudentChange = this.handleStudentChange.bind(this);
    this.handleSubjectChange = this.handleSubjectChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
    this.handleGradeChange = this.handleGradeChange.bind(this);
    HighchartsMore(Highcharts);
  }

  componentDidMount() {
    this.setState({loading: true});
    ApiService.getStudents().then((response) => response.json()).then((responseJson) => {
        let studentIds = responseJson.studentIds.map(obj => { return { value: obj.student_id, display: obj.student_id } });
        this.setState({ studentIds: [{ value:'', display: '(Select the Student)' }].concat(studentIds) });
    }).catch(err => console.log(err));
    ApiService.getSubjects().then((response) => response.json()).then((responseJson) => {
        let subjects = responseJson.subjects.map(obj => { return { value: obj.subject, display: obj.subject } });
        this.setState({ subjects: [{ value:'', display: '(Select the subject)' }].concat(subjects) });
    }).catch(err => console.log(err));
    ApiService.getYears().then((response) => response.json()).then((responseJson) => {
        let years = responseJson.years.map(obj => { return { value: obj.calandar_year, display: obj.calandar_year } });
        this.setState({ years: [{ value:'', display: '(Select the year)' }].concat(years) });   
        this.setState({loading: false});
    }).catch(err => console.log(err));
    let grades = [];
    for(let i=1;i<=12;i++){
        grades.push({value: i, display: i});
    }
    this.setState({ grades: [{ value:'', display: '(Select the grade)' }].concat(grades) });
    this.callApiServiceForCharts();
  }

  handleStudentChange(event){
     if(event.target.value !== ''){
        this.setState(prevState => ({
            filters:{
                ...prevState.filters,
                student_id: event.target.value
            }
        }), () => {
            this.callApiServiceForCharts();
        });
     }else{
         var array = this.state.filters;
        this.setState(prevState => ({
            filters: _.without( array, 'student_id' )
        }), () => {
            this.callApiServiceForCharts();
        });
     }
  }
  handleSubjectChange(event){
    if(event.target.value !== ''){
        this.setState(prevState => ({
            filters:{
                ...prevState.filters,
                subject: event.target.value
            }
        }), () => {
            this.callApiServiceForCharts();
        });
    }else{
        var array = this.state.filters;
       this.setState(prevState => ({
           filters: _.without( array, 'subject' )
       }), () => {
           this.callApiServiceForCharts();
       });
    }
  }
  handleYearChange(event){
    if(event.target.value !== ''){
        this.setState(prevState => ({
            filters:{
                ...prevState.filters,
                calandar_year: event.target.value
            }
        }), () => {
            this.callApiServiceForCharts();
        });
    }else{
        var array = this.state.filters;
       this.setState(prevState => ({
           filters: _.without( array, 'calandar_year' )
       }), () => {
           this.callApiServiceForCharts();
       });
    }
  }
  handleGradeChange(event){
      if(event.target.value !== ''){
        this.setState(prevState => ({
            filters:{
                ...prevState.filters,
                grade: event.target.value
            }
        }), () => {
            this.callApiServiceForCharts();
        });
      }else{
        var array = this.state.filters;
        this.setState(prevState => ({
            filters: _.without( array, 'grade' )
        }), () => {
            this.callApiServiceForCharts();
        });
      }
  }

  callApiServiceForCharts(){
    ApiService.getWhiskerChartData(this.state.filters).then((response) => response.json()).then((responseJson) => {
        let subjectList = [];
        let series = responseJson.chartData.map(function(obj, index){
            subjectList.push(obj.subject);
            let dataset = [];
            dataset.push(obj.min_mark);
            dataset.push(obj.Q1);
            dataset.push(obj.median);
            dataset.push(obj.Q3);
            dataset.push(obj.max_mark);
            return dataset;
        });
        this.setState({
            whiskerChartSeries: series,
            whiskerChartSubjectList : Array.from(new Set(subjectList))
        }, () => {
              this.generateWhiskerChart();
        });
    });
      if(this.state.filters.student_id && this.state.filters.calandar_year){
        this.apiGetColumnChartData();
      }
      if(this.state.filters.student_id){
        this.apiGetOverallMarkChartData();
        this.apiGetScatterChartData();
      }
  }

  apiGetColumnChartData(){
    this.setState({loading: true});
    ApiService.getColumnChartData(this.state.filters).then((response) => response.json()).then((responseJson) => {
        let subjectList = []; let series =[];
        let avgList = responseJson.chartData.avg.map(function(obj, index){
          /**Get the Average line */
            return obj.avg_mark;
        });
        let columnList = responseJson.chartData.column.map(function(obj,index){
            subjectList.push(obj.subject);
          /**Get the marks columns */
            return obj.mark;
        });
        series.push({ name: this.state.filters.student_id+'-'+this.state.filters.calandar_year,data: columnList},{ type: 'spline', name: 'average', data: avgList});
        this.setState({
            studentMarksChartSeries : series,
            studentMarksChartSubjectList : subjectList
        }, () => {
        this.setState({loading: false});
        this.generateStudentMarksChart();
        });
    }).catch(err => console.log(err));
  }

  apiGetOverallMarkChartData(){
    this.setState({loading: true});
    ApiService.getOverallMarkChartData(this.state.filters).then((response) => response.json()).then((responseJson) => {
        let tempList = []; let yearList = []; let subjectList = [];
        // let subjectMarks = new Promise(function(resolve, reject) {
            responseJson.chartData.map(function(obj, index){
                subjectList.push(obj.subject);
                tempList[obj.subject] = ( typeof tempList[obj.subject] != 'undefined' && tempList[obj.subject] instanceof Array ) ? tempList[obj.subject] : [];
                tempList[obj.subject].push(obj.mark);
                var year = obj.calandar_year+'-'+obj.semester;
                yearList.push(year);
            });
            // resolve(tempList);
        // }); 
        // subjectMarks.then(function(markList){
            let interateList = Array.from(new Set(subjectList));
            let series = interateList.map(function(subject, index){
                return {name: subject, data: tempList[subject]};
            })
        // });
        this.setState({
            overallMarksChartSeries: series,
            overallMarksChartYearList : Array.from(new Set(yearList))
        }, () => {
            this.setState({loading: false});
            this.generateOverallMarksChart();
        });
    });
  }

  apiGetScatterChartData(){
    this.setState({loading: true});
    ApiService.getScatterChartData(this.state.filters).then((response) => response.json()).then((responseJson) => {
        let series = responseJson.chartData.map(function(obj, index){
          /**Get the marks data */
            return obj.mark;
        });
        this.setState({
            scatterChartSeries : series
        }, () =>{
            this.setState({loading: false});
            this.generateWhiskerChart();
        });
    }).catch(err => console.log(err));
  }

  generateOverallMarksChart(){
    this.setState({ _overallMarksChartOptions: {
            title: {
                text: 'Student Marks for all the years'
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'Mark(s)'
                }
            },
            xAxis: {
                categories: this.state.overallMarksChartYearList
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            // plotOptions: {
            //     series: {
            //         label: {
            //             connectorAllowed: false
            //         },
            //         pointStart: 2010
            //     }
            // },
            series: this.state.overallMarksChartSeries,
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            },
            credits: {
                enabled: false
            }
        },
        get overallMarksChartOptions() {
            return this._overallMarksChartOptions;
        },
        set overallMarksChartOptions(value) {
            this._overallMarksChartOptions = value;
        }
    });
  }

  generateStudentMarksChart(){
    this.setState({ _studentMarksChartOptions: {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Student Subject marks'
            },
            xAxis: {
                categories: this.state.studentMarksChartSubjectList,
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'Mark(s)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: this.state.studentMarksChartSeries,
            credits: {
                enabled: false
            }
        },
        get studentMarksChartOptions() {
                    return this._studentMarksChartOptions;
                },
        set studentMarksChartOptions(value) {
                    this._studentMarksChartOptions = value;
                },
        });
  }

  generateWhiskerChart(){
    this.setState({ _studentWhiskerChartOptions: {
        chart: {
            type: 'boxplot'
        },
        title: {
            text: 'Student Marks variation'
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: this.state.whiskerChartSubjectList,
            title: {
                text: 'Subjects'
            }
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Mark(s)'
            }
        },
        series: [{
            name: 'Observations',
            data : this.state.whiskerChartSeries,
            tooltip: {
                headerFormat: '<em>{point.key}</em><br/>'
            }
        },
        {
            name: 'Student Mark',
            type: 'scatter',
            data: this.state.scatterChartSeries,
            accessibility: {
                exposeAsGroupOnly: true
            },
            marker: {
                radius: 1.5
            },
            tooltip: {
                headerFormat: '<em>{point.key}</em><br/>'
            }
        }
        ],
        credits: {
            enabled: false
        }},
        get studentWhiskerChartOptions() {
            return this._studentWhiskerChartOptions;
        },
        set studentWhiskerChartOptions(value) {
            this._studentWhiskerChartOptions = value;
        }
    });
  }

  render(){

      return(
        <div className="row">
            <Card style={{ width: '100%' }}>
                <Card.Body>
                    <form>
                        <div className="row">
                            <div className="col-md-2">
                                <label className="form-label">Student ID</label>
                                <select className="form-control" type="text" onChange={this.handleStudentChange}>
                                    {this.state.studentIds.map((Student, keyIndex) => {
                                        return (
                                        <option key={Student.value} value={Student.value}>{Student.display}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="col-md-2">
                            <label className="form-label">Year</label>
                                <select className="form-control" type="text" onChange={this.handleYearChange}>
                                    {this.state.years.map((year, keyIndex) => {
                                        return (
                                        <option key={year.value} value={year.value}>{year.display}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="col-md-2">
                            <label className="form-label">Grade</label>
                                <select className="form-control" type="text" onChange={this.handleGradeChange}>
                                    {this.state.grades.map((grade, keyIndex) => {
                                        return (
                                        <option key={grade.value} value={grade.value}>{grade.display}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="col-md-3">
                            <label className="form-label">Subject</label>
                                <select className="form-control" type="text" onChange={this.handleSubjectChange}>
                                    {this.state.subjects.map((Subject, keyIndex) => {
                                        return (
                                        <option key={Subject.value} value={Subject.value}>{Subject.display}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="col-md-2" style={{marginTop: '10px'}}>
                                {this.state.loading?
                                <Loader type="ThreeDots" color="#2BAD60" /> : null }
                            </div>
                        </div>
                    </form>
                </Card.Body>
            </Card>
            <Card style={{ width: '100%', margin: '25px' }}>
                <Card.Body>
                    <HighchartsReact
                    highcharts={Highcharts}
                    options={this.state.studentWhiskerChartOptions}
                    />
                </Card.Body>
            </Card>
            <Card style={{ width: '100%', margin: '25px' }}>
                <Card.Body>
                    <HighchartsReact
                    highcharts={Highcharts}
                    options={this.state.studentMarksChartOptions}
                    />
                </Card.Body>
            </Card>
            <Card style={{ width: '100%', margin: '25px' }}>
                <Card.Body>
                    <HighchartsReact
                    highcharts={Highcharts}
                    options={this.state.overallMarksChartOptions}
                    />
                </Card.Body>
            </Card>
        </div>
      )
    }
}
export default Home;