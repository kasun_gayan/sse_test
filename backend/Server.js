const express = require('express');
const bodyParser =  require('body-parser');
const querystring = require('querystring');
const url = require('url');

const {getStudentIds, getSubjects, getYears, getColumnChart, getOverallMarksChart, getWhiskerChart, getScatterChart} = require('./services/StudentService');

async function startServer() {
    const app = express();
    app.use(bodyParser.json());
    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin','*');
        res.setHeader(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        );
        res.setHeader(
            'Access-Control-Allow-Methods',
            'GET, POST, PATCH, DELETE, OPTIONS'
        );
        next();
    });

    app.get('/students', async (req, res, next) => {
        getStudentIds().then(function(results){
            res.json({ studentIds: results });
        });
    });
    app.get('/subjects', async (req, res, next) => {
        getSubjects().then(function(results){
            res.json({ subjects: results });
        });
    });
    app.get('/years', async (req, res, next) => {
        getYears().then(function(results){
            res.json({ years: results });
        });
    });
    app.post('/column_chart',  async (req, res, next) => {
        const filters = req.body;
        getColumnChart(filters).then(function(results){
            res.json({ chartData: results });
        });
    });
    app.post('/oervall_marks_chart',  async (req, res, next) => {
        const filters = req.body;
        getOverallMarksChart(filters).then(function(results){
            res.json({ chartData: results });
        });
    });
    app.post('/whisker_chart',  async (req, res, next) => {
        const filters = req.body;
        getWhiskerChart(filters).then(function(results){
            res.json({ chartData: results });
        });
    });
    app.post('/scatter_chart',  async (req, res, next) => {
        const filters = req.body;
        getScatterChart(filters).then(function(results){
            res.json({ chartData: results });
        });
    });

    app.timeout = 0;
    app.listen(5000);
}
startServer();
