DROP TABLE IF EXISTS student_marks;
CREATE TABLE IF NOT EXISTS student_marks (id INT AUTO_INCREMENT, student_id TINYINT(2), subject VARCHAR(30), mark TINYINT(2), semester CHAR(1), student_name VARCHAR(40), grade TINYINT(2), calandar_year SMALLINT(4), PRIMARY KEY(id, student_id, subject,semester,calandar_year));
SELECT * FROM sse_test.student_marks;
OPTIMIZE TABLE student_marks;

select avg(mark), subject from student_marks group by Subject;
SELECT mark, subject FROM student_marks group by subject;
SELECT mark, subject, calandar_year, semester from student_marks where student_id = 1 group by calandar_year, subject, semester;

SELECT mark,student_id,subject from student_marks
GROUP BY subject
HAVING SUM(SIGN(1-SIGN(`mark`)))/COUNT(*) > .5
LIMIT 1;

SELECT subject, median(mark) OVER (PARTITION BY subject), min(mark), max(mark) FROM student_marks group by subject;

SET @number_of_rows := (SELECT COUNT(*) FROM student_marks);
SET @quartile := (ROUND(@number_of_rows*0.25));
SET @sql_q1 := (CONCAT('(SELECT "Q1" AS quartile_name , mark, subject FROM student_marks ORDER BY mark DESC LIMIT 1 OFFSET ', @quartile,')'));
SET @sql_q3 := (CONCAT('( SELECT "Q3" AS quartile_name , mark, subject FROM student_marks ORDER BY mark ASC LIMIT 1 OFFSET ', @quartile,');'));
SET @sql := (CONCAT(@sql_q1,' UNION ',@sql_q3));
PREPARE stmt1 FROM @sql;
EXECUTE stmt1;

SELECT subject,
min(mark), max(mark),
cast(substring_index(
        substring_index(
            group_concat(`mark` 
                          order by `mark` 
                          ASC separator ','
                        ),',',((0.50 * count(0)) + 1)
                       ),',',-(1)
                    ) as decimal(18,8)
    ) AS `Median`,
cast(substring_index(
        substring_index(
            group_concat(`mark` 
                          order by `mark` 
                          ASC separator ','
                        ),',',((0.25 * count(0)) + 1)
                       ),',',-(1)
                     ) as decimal(18,8)
    ) AS `Q1`,
cast(substring_index(
        substring_index(
            group_concat(`mark` 
                          order by `mark` 
                          ASC separator ','
                        ),',',((0.75 * count(0)) + 1)
                       ),',',-(1)
                    ) as decimal(18,8)
    ) AS `Q3`
FROM student_marks group by subject;

