const faker = require('faker')
const mysqlClient = require('./config/database');

var dropSql = "DROP TABLE IF EXISTS student_marks";
mysqlClient.query(dropSql, function (err, result) {
    if (err) console.log ('error', err.message, err.stack)
    else console.log("Table Droped");
});
var createSql = "CREATE TABLE IF NOT EXISTS student_marks (id INT AUTO_INCREMENT, student_id TINYINT(2), subject VARCHAR(30), mark TINYINT(2), semester CHAR(1), student_name VARCHAR(40), grade TINYINT(2), calandar_year SMALLINT(4), PRIMARY KEY(id, student_id, subject,semester,calandar_year))";
mysqlClient.query(createSql, function (err, result) {
    if (err) console.log ('error', err.message, err.stack)
    else console.log("Table Created");
});

const subjects = ['Business & management', 'Computer Science', 'Data Analysis & Statistics', 'Architecture', 'Biology', 'Chemistry', 'Communication', 'Economics', 
            'Engineering', 'Electronicts','Ethics', 'History', 'Math', 'Music', 'Literature', 'Physics', 'Science', 'Health', 'Philosophy', 'Religion', 'Art', 'Archaeology', 'Law',
            'Social Studies', 'English', 'Accounting', 'Anatomy', 'Criminology', 'Cryptography', 'Dance', 'Drama', 'Fashion Studies', 'Food', 'Geography', 'Politcis', 'Quantitative',
            'Sports', 'Software Development', 'Statistics', 'chineese', 'Japaneese', 'Journalism', 'Italian', 'Sinhala', 'French', 'Latin', 'Home Science','Hindi', 'Islam', 'Agriculture'
];
const years = [2001,2002,2003,2004,2005,2006,2007,2008,2009,2010];
count = 0;
/** Testest the application with 1000(10 years x 2 semesters x 50 subjects) = (1 Million) */
/** Script fails after 2 million. Need to optimize the migration process. not yet done */
for(let i=1;i<=20;i++){
    const name = faker.name.firstName()+' '+faker.name.lastName();
    years.forEach(function(year, grade ){       
    var values = [];
        for(let semester=1;semester<=2;semester++){
            subjects.forEach(subject => { 
                /** has to multiply subject by 500 to generate 10 million*/
                /** as its not possible to view in the chart, removed multiplication */
                for(let si=1;si<=1;si++){
                    const mark = faker.random.number({
                        'min': 5,
                        'max': 98
                    });
                    values.push([`${i}`, `${subject+' '+si}`, `${mark}`, `${semester}`, `${name}`, `${grade+1}`, `${year}`]);
                    count++;
                }; 
            });
        }
        console.log(count);
        var insetSql = "INSERT INTO student_marks (student_id,subject,mark,semester,student_name,grade,calandar_year) VALUES ?";
        mysqlClient.query(insetSql, [values], function (err, result) {
            if (err) console.log ('error', err.message, err.stack)
            else console.log("Table rows inserted");
        });
    });
//   handleDisconnect();
    mysqlClient.query('select 1 + 1', (err, rows) => { /* */ });
}
var optimizedSql = "OPTIMIZE TABLE student_marks";
console.log("Table optimizing .....");
mysqlClient.query(optimizedSql, function (err, result) {
    if (err) console.log ('error', err.message, err.stack)
    else console.log("Table optimized");
});

// function handleDisconnect() {
//     mysqlClient = mysql.createConnection(mysqlConfig); 
  
//     mysqlClient.connect(function(err) {
//       if(err) {
//         console.log('error when connecting to db:', err);
//         setTimeout(handleDisconnect, 2000);
//       }
//     });
//     mysqlClient.on('error', function(err) {
//       console.log('db error', err);
//       if(err.code === 'PROTOCOL_CONNECTION_LOST') {
//         handleDisconnect();
//       } else {
//         throw err;
//       }
//     });
//   }
