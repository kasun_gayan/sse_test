
var mysql   = require('mysql');

var mysqlConfig = { host: 'localhost', user: 'root', password: '', database: 'sse_test' };
var mysqlClient = mysql.createConnection(mysqlConfig); // This is the global MySQL client

module.exports = mysqlClient;