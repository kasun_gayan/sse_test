const {getStudentIdsfromDB,getSubjectsfromDB,getYearsfromDB,getColumnChartfromDB,getOverallMarksChartfromDB,getWhiskerChartfromDB,getScatterChartfromDB} = require('../models/StudentMarks');

async function getStudentIds(){
    return new Promise(function(resolve, reject){
        getStudentIdsfromDB().then(function (results) {
            resolve(results);
        });
    });
}
async function getSubjects(){
    return new Promise(function(resolve, reject){
        getSubjectsfromDB().then(function (results) {
            resolve(results);
        });
    });
}
async function getYears(){
    return new Promise(function(resolve, reject){
        getYearsfromDB().then(function (results) {
            resolve(results);
        });
    });
}
async function getColumnChart(filters){
    return new Promise(function(resolve, reject){
        getColumnChartfromDB(filters).then(function (results) {
            resolve(results);
        });
    });
}
async function getOverallMarksChart(filters){
    return new Promise(function(resolve, reject){
        getOverallMarksChartfromDB(filters).then(function (results) {
            resolve(results);
        });
    });
}
async function getWhiskerChart(filters){
    return new Promise(function(resolve, reject){
        getWhiskerChartfromDB(filters).then(function (results) {
            resolve(results);
        });
    });
}
async function getScatterChart(filters){
    return new Promise(function(resolve, reject){
        getScatterChartfromDB(filters).then(function (results) {
            resolve(results);
        });
    });
}

module.exports = {
    getStudentIds,
    getSubjects,
    getYears,
    getColumnChart,
    getOverallMarksChart,
    getWhiskerChart,
    getScatterChart
}