import config from './../config';

export default {
    getStudents: async function() {
        try{
            console.info(`Fetch Student Info >>> ${config.api_url}/students`);
            return fetch(`${config.api_url}/students`, {
                method: 'GET',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                }
            });
        }catch(e){
            console.error(e);
        }
    },
    getSubjects: async function() {
        try{
            console.info(`Fetch Subject Info >>> ${config.api_url}/subjects`);
            return fetch(`${config.api_url}/subjects`, {
                method: 'GET',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                }
            });
        }catch(e){
            console.error(e);
        }
    },
    getYears: async function() {
        try{
            console.info(`Fetch Year Info >>> ${config.api_url}/years`);
            return fetch(`${config.api_url}/years`, {
                method: 'GET',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                }
            });
        }catch(e){
            console.error(e);
        }
    },
    getColumnChartData: async function(filters) {
        try{
            console.info(`Fetch ColumnChart Info >>> ${config.api_url}/column_chart`);
            return fetch(`${config.api_url}/column_chart`, {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(filters)
            });
        }catch(e){
            console.error(e);
        }
    },
    getOverallMarkChartData: async function(filters) {
        try{
            console.info(`Fetch OverallMarksChart Info >>> ${config.api_url}/overall_marks_hart`);
            return fetch(`${config.api_url}/oervall_marks_chart`, {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(filters)
            });
        }catch(e){
            console.error(e);
        }
    },
    getWhiskerChartData: async function(filters) {
        try{
            console.info(`Fetch WhiskerChart Info >>> ${config.api_url}/whisker_chart`);
            return fetch(`${config.api_url}/whisker_chart`, {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(filters)
            });
        }catch(e){
            console.error(e);
        }
    },
    getScatterChartData: async function(filters) {
        try{
            console.info(`Fetch ScatterChart Info >>> ${config.api_url}/scatter_chart`);
            return fetch(`${config.api_url}/scatter_chart`, {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(filters)
            });
        }catch(e){
            console.error(e);
        }
    },

}