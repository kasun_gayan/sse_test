const mysqlClient = require('./../config/database');

  async function getStudentIdsfromDB() {
    return new Promise(function(resolve, reject){
        mysqlClient.query("SELECT distinct student_id FROM student_marks", function(err, result, fields){
            if (err) console.log ('error', err.message, err.stack)
            resolve(result);
        });
    });
  }
  async function getSubjectsfromDB(filters){
    return new Promise(function(resolve, reject){
        mysqlClient.query("SELECT distinct subject FROM student_marks", function(err, result, fields){
            if (err) console.log ('error', err.message, err.stack)
            resolve(result);
        });
    });
  }
  async function getYearsfromDB(filters){
    return new Promise(function(resolve, reject){
        mysqlClient.query("SELECT distinct calandar_year FROM student_marks", function(err, result, fields){
            if (err) console.log ('error', err.message, err.stack)
            resolve(result);
        });
    });
  }
  /** Avg Filter based on the student_id,subject */
  /** Column Filter based on the student_id,calandar_year,grade,subject */
  async function getColumnChartfromDB(filters){
    return new Promise(function(resolve, reject){
        var avgConditions = buildAvgConditions(filters);
        mysqlClient.query(`SELECT avg(mark) as avg_mark, semester, subject FROM student_marks WHERE ${avgConditions.where} GROUP BY subject,semester`,avgConditions.values, function(err, resultAvg, fields){
            if (err) console.log ('error', err.message, err.stack)
              var whereConditions = buildColumnConditions(filters, 'column');
              mysqlClient.query(`SELECT mark, subject FROM student_marks WHERE ${whereConditions.where} GROUP BY subject,semester`, whereConditions.values, function(err, resultYear, fields){
                if (err) console.log ('error', err.message, err.stack)
                resolve({avg:resultAvg, column:resultYear});
              });
        });
    });
  }
  /**Filter only by student_id */
  async function getOverallMarksChartfromDB(filters){
      return new Promise(function(resolve, reject){
            mysqlClient.query('SELECT mark, subject, calandar_year, semester FROM student_marks WHERE student_id = ? GROUP BY calandar_year, subject, semester', [filters.student_id], function(err, result, fields){
              if (err) console.log ('error', err.message, err.stack)
              resolve(result);
            });
      });
  }
  /**No filter added */
  async function getWhiskerChartfromDB(filters){
      return new Promise(function(resolve, reject){
            var whereConditions = buildColumnConditions(filters, 'whisker');
            var SQL = `SELECT subject, min(mark) AS min_mark, max(mark) AS max_mark,cast(substring_index(substring_index(group_concat(\`mark\` order by \`mark\` ASC separator ',' ),',',((0.50 * count(0)) + 1)),',',-(1)) as decimal(18,8)) AS \`median\`,cast(substring_index(substring_index(group_concat(\`mark\` order by \`mark\` ASC separator ','),',',((0.25 * count(0)) + 1)),',',-(1)) as decimal(18,8)) AS \`Q1\`,cast(substring_index(substring_index(group_concat(\`mark\` order by \`mark\` ASC separator ','),',',((0.75 * count(0)) + 1)),',',-(1)) as decimal(18,8)) AS \`Q3\` FROM student_marks WHERE ${whereConditions.where} GROUP BY subject`;
            mysqlClient.query(SQL, whereConditions.values, function(err, result, fields){
              if (err) console.log ('error', err.message, err.stack)
              resolve(result);
            });
      });
  }
  /** Filter based on the student_id,calandar_year,grade,subject */
  async function getScatterChartfromDB(filters){
    return new Promise(function(resolve, reject){
      var whereConditions = buildColumnConditions(filters, 'scatter');
      mysqlClient.query(`SELECT mark,subject FROM student_marks WHERE ${whereConditions.where} GROUP BY subject`, whereConditions.values, function(err, result, fields){
        if (err) console.log ('error', err.message, err.stack)
        resolve(result);
      });
});
  }

  function buildAvgConditions(params){
    var conditions = [];
    var values = [];
    if (typeof params.student_id !== 'undefined' && params.student_id !=='') {
      conditions.push("student_id = ?");
      values.push(parseInt(params.student_id));
    }
    // if (typeof params.calandar_year !== 'undefined' && params.calandar_year !=='') {
    //   conditions.push("calandar_year = ?");
    //   values.push(parseInt(params.calandar_year));
    // }
    // if (typeof params.grade !== 'undefined' && params.grade !=='') {
    //   conditions.push("grade = ?");
    //   values.push(parseInt(params.grade));
    // }
    if (typeof params.subject !== 'undefined' && params.subject !=='') {
      conditions.push("subject LIKE ?");
      values.push("%" + params.subject + "%");
    }
    return {
      where: conditions.length ?
               conditions.join(' AND ') : '1',
      values: values
    };
  }

  function buildColumnConditions(params, mode){
    var conditions = [];
    var values = [];
    if (['column','scatter'].indexOf(mode) >= 0) {
      if (typeof params.student_id !== 'undefined' && params.student_id !=='') {
        conditions.push("student_id = ?");
        values.push(parseInt(params.student_id));
      }
      if (typeof params.calandar_year !== 'undefined' && params.calandar_year !=='') {
        conditions.push("calandar_year = ?");
        values.push(parseInt(params.calandar_year));
      }
      if (typeof params.grade !== 'undefined' && params.grade !=='') {
        conditions.push("grade = ?");
        values.push(parseInt(params.grade));
      }
      if (typeof params.subject !== 'undefined' && params.subject !=='') {
        conditions.push("subject LIKE ?");
        values.push("%" + params.subject + "%");
      }
    }
    return {
      where: conditions.length ?
               conditions.join(' AND ') : '1',
      values: values
    };
  }

module.exports = {
  getSubjectsfromDB,
  getStudentIdsfromDB,
  getYearsfromDB,
  getColumnChartfromDB,
  getOverallMarksChartfromDB,
  getWhiskerChartfromDB,
  getScatterChartfromDB
}; 